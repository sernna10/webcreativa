<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tasks;

class TaskController extends Controller
{
    public function create()
    {
        return view('task.create');
    }

    public function edit($id)
    {
        return view('task.update', [
            'task' => Tasks::findOrFail($id)
        ]);
    }

    public function store()
    {
        request()->validate([
            'name' => ['required', 'string', 'min:5', 'max:20'],
            'start_date' => ['required', 'date'],
            'end_date' => ['required', 'date'],
            'status' => ['required', 'string']
        ]);

        $status;

        if (request('status') == 'iniciada') {
            $status = 'INICIADA';
        } else if (request('status') == 'proceso') {
            $status = 'EN PROCESO';
        } else if (request('status') == 'cancelada') {
            $status = 'CANCELADA';
        } else if (request('status') == 'completada') {
            $status = 'COMPLETADA';
        }else {
            return back();
        }

        Tasks::create([
        'name' => request('name'),
        'start_date' => request('start_date'),
        'end_date' => request('end_date'),
        'status' => $status
        ]);

        return redirect()->route('home');
    }

    public function update(Tasks $task)
    {
        request()->validate([
            'name' => ['required', 'string', 'min:5', 'max:20'],
            'start_date' => ['required', 'date'],
            'end_date' => ['required', 'date'],
            'status' => ['required', 'string', 'max:15']
        ]);

        $status;

        if (request('status') == 'iniciada') {
            $status = 'INICIADA';
        } else if (request('status') == 'proceso') {
            $status = 'EN PROCESO';
        } else if (request('status') == 'cancelada') {
            $status = 'CANCELADA';
        } else if (request('status') == 'completada') {
            $status = 'COMPLETADA';
        }else {
            return back();
        }
        
        $task->update([
            'name' => request('name'),
            'start_date' => request('start_date'),
            'end_date' => request('end_date'),
            'status' => $status
        ]);

        return redirect()->route('home', $task);
    }

    public function destroy(Tasks $task)
    {
        $task->delete();

        return redirect()->route('home');
    }
}
