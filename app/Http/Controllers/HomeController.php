<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tasks;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $task = Tasks::get();

        $totalIniciada = Tasks::where('status','=','INICIADA')->count();
        $totalProceso = Tasks::where('status','=','EN PROCESO')->count();
        $totalCancelada = Tasks::where('status','=','CANCELADA')->count();
        $totalCompletada = Tasks::where('status','=','COMPLETADA')->count();
        
        return view('home', [
            'task' => $task,
            'totalIniciada' => $totalIniciada,
            'totalProceso' => $totalProceso,
            'totalCancelada' => $totalCancelada,
            'totalCompletada' => $totalCompletada
        ]);
    }
}
