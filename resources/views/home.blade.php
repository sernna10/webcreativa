@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-7 rounded border border-3">

                <h1 class="text-center">LISTA DE TAREAS</h1>

                <a href="{{ route('task.create') }}" class="btn btn-primary mb-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-plus-square" viewBox="0 0 16 16">
                        <path
                            d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                        <path
                            d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                    </svg>
                    Agregar tarea
                </a>

                <table class="table table-responsive table-hover table-striped">
                    <thead class="thead-dark">
                        <tr class="table-bordered">
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Fecha de inicio</th>
                            <th scope="col">Fecha fin</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($task as $data)
                            <tr class="table-bordered">
                                <th scope="row">{{ $data->id }}</th>
                                <td>{{ $data->name }}</td>
                                <td>{{ $data->start_date }}</td>
                                <td>{{ $data->end_date }}</td>
                                <td>{{ $data->status }}</td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('task.edit', $data->id) }}" title="Editar">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-wrench"
                                            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M.102 2.223A3.004 3.004 0 0 0 3.78 5.897l6.341 6.252A3.003 3.003 0 0 0 13 16a3 3 0 1 0-.851-5.878L5.897 3.781A3.004 3.004 0 0 0 2.223.1l2.141 2.142L4 4l-1.757.364L.102 2.223zm13.37 9.019L13 11l-.471.242-.529.026-.287.445-.445.287-.026.529L11 13l.242.471.026.529.445.287.287.445.529.026L13 15l.471-.242.529-.026.287-.445.445-.287.026-.529L15 13l-.242-.471-.026-.529-.445-.287-.287-.445-.529-.026z" />
                                        </svg>
                                    </a>
                                    <form action="{{ route('task.destroy', $data->id) }}" method="post">
                                        @csrf @method('DELETE')
                                        <button class="btn btn-danger" title="Eliminar">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash"
                                                fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                                <path fill-rule="evenodd"
                                                    d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <h2 class="m-2">No hay datos que mostrar.</h2>
                        @endforelse

                    </tbody>
                </table>

                <hr>


                <button type="button" class="btn btn-primary mb-3">
                    Total INICIADAS <span class="badge bg-dark">{{ $totalIniciada }}</span>
                </button>

                <button type="button" class="btn btn-secondary mb-3">
                    Total EN PROCESO <span class="badge bg-dark">{{ $totalProceso }}</span>
                </button>

                <button type="button" class="btn btn-danger mb-3">
                    Total CANCELADAS <span class="badge bg-dark">{{ $totalCancelada }}</span>
                </button>

                <button type="button" class="btn btn-success mb-3">
                    Total COMPLETADAS <span class="badge bg-dark">{{ $totalCompletada }}</span>
                </button>

            </div>
        </div>
    </div>
@endsection
