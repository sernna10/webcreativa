@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card">
                    <div class="card-header">{{ __('Actualizar tarea') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('task.update', $task) }}">
                            @csrf
                            @method('PATCH')
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ $task->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="start_date"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Fecha de inicio') }}</label>

                                <div class="col-md-6">
                                    <input id="start_date" type="date"
                                        class="form-control @error('start_date') is-invalid @enderror" name="start_date"
                                        value="{{ $task->start_date }}" required autocomplete="start_date" autofocus>

                                    @error('start_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="end_date"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Fecha final') }}</label>

                                <div class="col-md-6">
                                    <input id="end_date" type="date"
                                        class="form-control @error('end_date') is-invalid @enderror" name="end_date"
                                        value="{{ $task->end_date }}" required autocomplete="end_date" autofocus>

                                    @error('end_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="status"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Estado') }}</label>

                                <div class="col-md-6">

                                    @if ($task->status == 'INICIADA')
                                        <select id="status" class="form-select form-control"
                                            aria-label="Default select example" @error('status') is-invalid @enderror"
                                            name="status" required autocomplete="status">
                                            <option value="iniciada" selected>INICIADA</option>
                                            <option value="proceso">EN PROCESO</option>
                                            <option value="cancelada">CANCELADA</option>
                                            <option value="completada">COMPLETADA</option>
                                        </select>
                                    @elseif ($task->status == 'EN PROCESO')
                                        <select id="status" class="form-select form-control"
                                            aria-label="Default select example" @error('status') is-invalid @enderror"
                                            name="status" required autocomplete="status">
                                            <option value="iniciada">INICIADA</option>
                                            <option value="proceso" selected>EN PROCESO</option>
                                            <option value="cancelada">CANCELADA</option>
                                            <option value="completada">COMPLETADA</option>
                                        </select>
                                    @elseif ($task->status == 'CANCELADA')
                                        <select id="status" class="form-select form-control"
                                            aria-label="Default select example" @error('status') is-invalid @enderror"
                                            name="status" required autocomplete="status">
                                            <option value="iniciada">INICIADA</option>
                                            <option value="proceso">EN PROCESO</option>
                                            <option value="cancelada" selected>CANCELADA</option>
                                            <option value="completada">COMPLETADA</option>
                                        </select>
                                    @elseif ($task->status == 'COMPLETADA')
                                        <select id="status" class="form-select form-control"
                                            aria-label="Default select example" @error('status') is-invalid @enderror"
                                            name="status" required readonly autocomplete="status">
                                            <option value="completada" selected>COMPLETADA</option>
                                        </select>
                                    @endif

                                    @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Actualizar') }}
                                    </button>
                                    <a class="btn btn-danger" href="{{ route('home') }}">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
