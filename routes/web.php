<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// CREATE TASK
Route::get('/crear', 'TaskController@create')->name('task.create');
Route::post('/tarea/crear', 'TaskController@store')->name('task.store');

// UPDATE TASK
Route::get('/tarea/{task}/editar', 'TaskController@edit')->name('task.edit');
Route::patch('/tarea/{task}', 'TaskController@update')->name('task.update');

// DELETE TASK
Route::delete('/tarea/{task}', 'TaskController@destroy')->name('task.destroy');